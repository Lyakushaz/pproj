from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.mixins import *

from apps.test.models import Test
from apps.test.serializers import TestSerializer


class TestViewSet(CreateModelMixin, ListModelMixin, RetrieveModelMixin, GenericViewSet):
    serializer_class = TestSerializer
    queryset = Test.objects.all()
