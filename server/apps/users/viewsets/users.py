# TODO: импорты не по pep8
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.mixins import *
from django.contrib.auth.models import User
from apps.users.serializers.users import UserSerializer
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
from django.db.models.signals import post_save



class UserViewSet(CreateModelMixin, GenericViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def create(self):
        # TODO: дописать
        pass

    @receiver(post_save, sender=User)
    def create_auth_token(sender, instance=None, created=False, **kwargs):
        if created:
            # TODO: это у тебя будет в методе create. К тому же ты это оформил как сигнал. Сигнал явно не должен быть
            # методом вьюшки. Сигналы это отдельная сущность. Смотри как это реализвоано в apps.test.models.signals
            Token.objects.create(user=instance)

