from rest_framework import serializers
from django.contrib.auth.models import User



class UserSerializer(serializers.ModelSerializer):
    token = serializers.CharField(allow_blank=True, read_only=True, source='auth_token.key') # TODO: выпелить
    password = serializers.CharField(allow_blank=False, write_only=True) # TODO: password можно задать как write_only через extra_kwargs

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'token',) # TODO: выпелить token

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

