from rest_framework import routers

from apps.test.viewsets import TestViewSet
from apps.users.viewsets.users import UserViewSet

router = routers.DefaultRouter()
router.register('test', TestViewSet, base_name='test')
router.register('users', UserViewSet, base_name='users')

